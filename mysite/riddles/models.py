from django.db import models
from django.contrib.auth.models import User
from django.dispatch import receiver
from django.db.models.signals import post_save

class Video(models.Model):
    name= models.CharField(max_length=200)
    rating=models.IntegerField()
    videoFile=models.FileField(upload_to="videos")
    user = models.ForeignKey(User, on_delete = models.CASCADE, blank=True, null=True)

class VideoTeg(models.Model):
    name= models.CharField(max_length=200)
    videos = models.ManyToManyField(Video)

class UserLike(models.Model):
    video=models.OneToOneField(Video, on_delete = models.CASCADE)
    users=models.ManyToManyField(User)

@receiver(post_save, sender=Video)
def create_video_userlike(sender, instance, created, **kwargs):
    if created:
        print(1)
        UserLike.objects.create(video=instance)

@receiver(post_save, sender=Video)
def save_video_userlike(sender, instance, **kwargs):
    instance.userlike.save()
