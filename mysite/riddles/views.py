from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect, HttpResponsePermanentRedirect
from django.core.serializers import serialize
from django.contrib import auth
from django.contrib.auth.models import User
from .models import Video, VideoTeg, UserLike
from django.shortcuts import render, redirect
from django.contrib.auth.forms import UserCreationForm
from django.views.decorators.csrf import csrf_exempt
import json
from django.http import JsonResponse
from .forms import ProfileForm
import json
from django.contrib.auth.decorators import login_required

def index(request):
    return render(request, 'popularvidios.html')


@login_required(login_url='/', redirect_field_name='')
def SaveProfile(request):
   saved = False
   
   if request.method == "POST":
      MyProfileForm = ProfileForm(request.POST, request.FILES)
      
      if MyProfileForm.is_valid():
         profile = Video()
         profile.name = MyProfileForm.cleaned_data["name"]
         profile.videoFile = MyProfileForm.cleaned_data["picture"]
         profile.rating=0
         profile.user=request.user
         profile.save()
         saved = True
   else:
      MyProfileForm = ProfileForm()
		
   return render(request, 'load.html', {'form': MyProfileForm})

@login_required(login_url='/', redirect_field_name='')
def myvideos(request):
    return render(request, 'myvideos.html')

def signup(request):
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponse(status=200)
        else:
            return render(request, 'signup.html', {'form': form})
    else:
        form = UserCreationForm()
    return render(request, 'signup.html', {'form': form})

def signin(request):
    if request.method == 'POST':
            userJson =  json.loads(request.body.decode('utf-8'))
            user = auth.authenticate(username=userJson['username'], password=userJson['password1'])
            if user is not None:
                auth.login(request, user)
                return HttpResponse(status=200)
            else:
                return HttpResponse(status=404)

def logout(request):
    auth.logout(request)
    return HttpResponse(status=200)

def likeVideo(request):
    if request.user.is_authenticated:
        idVideo = request.GET.get("id", -1)
        if not idVideo==-1:
            video = Video.objects.get(pk=idVideo)
            userLikeVideo=video.userlike.users.filter(username=request.user.username)
            if len(userLikeVideo)==0:
                video.rating+=1
                video.userlike.users.add(request.user)
                video.save()
                return HttpResponse(status=200)
            else:
                return HttpResponse(status=208)
        else:
            return HttpResponse(status=404)
    else:
        return HttpResponse(status=401)

def getVidiosPopularApi(request):
    videos=Video.objects.order_by("-rating")
    response= serialize('json', videos)
    return HttpResponse(response, content_type="application/json", status=200)

def myvideosapi(request):
    videos=Video.objects.filter(user=request.user)
    response= serialize('json', videos)
    return HttpResponse(response, content_type="application/json", status=200)

def removevideoapi(request):
    idVideo = request.GET.get("id", -1)
    if not idVideo==-1:
        video = Video.objects.get(pk=idVideo)
        if video.user==request.user or request.user.is_superuser:
            video.delete()
            return HttpResponse(status=200)
        else:
            return HttpResponse(status=403)
    else:
        return HttpResponse(status=404)

def userstatusapi(request):
    userStatus={}
    userStatus["status"]=request.user.is_superuser
    response = json.dumps(userStatus)
    return HttpResponse(response, content_type="application/json", status=200)

def users(request):
    if request.user.is_superuser:
        return render(request, 'users.html')
    else:
        return HttpResponseRedirect("/")

def getuserapi(request):
    if request.user.is_superuser:
        users=User.objects.all()
        response= serialize('json', users)
        return HttpResponse(response, content_type="application/json", status=200)
    else:
        return HttpResponse(status=403)

def removeuser(request):
    idUser= request.GET.get("id", -1)
    if not idUser==-1:
        user = User.objects.get(pk=idUser)
        if request.user.is_superuser:
            user.delete()
            return HttpResponse(status=200)
        else:
            return HttpResponse(status=403)
    else:
        return HttpResponse(status=404)