from django.contrib.auth.views import login, logout
from django.conf.urls import url, include
from django.contrib.auth.models import User
from . import views
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^api/login_in/$', views.signin, name='signin'),
    url(r'^api/logout/$', views.logout, name='logout'),
    url(r'^api/getvidiospopular/$', views.getVidiosPopularApi, name='getVidiosPopularApi'),
    url(r'^api/likevideo/', views.likeVideo, name='likeVideo'),
    url(r'^api/myvideosapi/', views.myvideosapi, name='myvideosapi'),
    url(r'^api/removevideoapi/', views.removevideoapi, name='removevideoapi'),
    url(r'^api/userstatusapi/', views.userstatusapi, name='userstatusapi'),
    url(r'^api/getuserapi/', views.getuserapi, name='getuserapi'),
    url(r'^api/removeuser/', views.removeuser, name='removeuser'),
    url(r'^registration/$', views.signup, name='signup'),
    url(r'^load/$', views.SaveProfile, name='load'),
    url(r'^myvideos/$', views.myvideos, name='myvideos'),
    url(r'^users/$', views.users, name='users'),
]+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)