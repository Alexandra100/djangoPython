$(document).ready(function () {
    getUserVideos();
})

function getUserVideos()
{
    var request = new XMLHttpRequest();
    request.open("GET", "/api/myvideosapi/", false);
    request.send();
    var myObj = JSON.parse(request.responseText);
    var x='';
    for (element of myObj) {
        x+='<div class="video" id="video'+element.pk+'">'
        x+='<div>'+element.fields.name+'</div>'
        x += '<div><video width="300" height="200" class="video-js" controls preload="none" data-setup="{}">';
        x +=  '<source src=../media/'+element.fields.videoFile+'/>';
        x +='</video></div> ';
        x+='<img src="../static/images/remove.png"  width="50" height="50" onclick="removeVideo('+element.pk+')">'
        x+='</div> '
    }
    document.getElementById("myvideos").innerHTML = x;
}

function removeVideo(id)
{
    $.ajax({
        url: '/api/removevideoapi?id='+id,
        statusCode: {
            200: function (response) {
               document.getElementById('video'+id).remove();
            }
        },
    });
}