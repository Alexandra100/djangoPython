$(document).ready(function () {
    getUsers();
})

var options = {
    year: 'numeric',
    month: 'long',
    day: 'numeric',
    timezone: 'UTC',
    hour: 'numeric',
    minute: 'numeric',
    second: 'numeric'
  };

function getUsers()
{
    var request = new XMLHttpRequest();
    request.open("GET", "/api/getuserapi/", false);
    request.send();
    var myObj = JSON.parse(request.responseText);
    var x='<tr><th>Логин</th><th>Дата регистрации</th><th>Статус</th><th>Действие</th></tr>';
    for (element of myObj) {
        x+='<tr id="user'+element.pk+'">';
        x+='<td>'+element.fields.username+'</td>';
        var msUTC = Date.parse(element.fields.date_joined);
        var msUTC=new Date(msUTC);
        x+='<td>'+msUTC.toLocaleString("ru", options)+'</td>';
        if (element.fields.is_superuser)
        {
            x+='<td>Администратор</td>';
        }
        else
        {
            x+='<td>Пользователь</td>';
        }
        x+='<td><img src="../static/images/remove.png"  width="50" height="50" onclick="removeUser('+element.pk+')"></td>';
        x+='</tr>';
    }
    document.getElementById("users").innerHTML = x;
}

function removeUser(id)
{
    $.ajax({
        url: '/api/removeuser?id='+id,
        statusCode: {
            200: function (response) {
               document.getElementById('user'+id).remove();
            }
        },
    });
}