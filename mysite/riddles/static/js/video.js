$(document).ready(function () {
    update();
    window.setInterval(updateLike, 5000);
})

function likeVideo(id)
{
    $.ajax({
        url: '/api/likevideo?id='+id,
        statusCode: {
            200: function (response) {
                document.getElementById("like"+id).innerHTML=Number(document.getElementById("like"+id).innerHTML)+1;
            }
        },
    });;
}

function update()
{
    var request = new XMLHttpRequest();
    request.open("GET", "/api/getvidiospopular/", false);
    request.send();
    var myObj = JSON.parse(request.responseText);
    request.open("GET", "/api/userstatusapi/", false);
    request.send();
    var userStatus=JSON.parse(request.responseText);
    var x='';
    for (element of myObj) {
        x+='<div class="video" id="video'+element.pk+'">'
        x+='<div>'+element.fields.name+'</div>'
        x += '<div><video width="300" height="200" class="video-js" controls preload="none" data-setup="{}">';
        x +=  '<source src="media/'+element.fields.videoFile+'"/>';
        x +='</video></div> ';
        x+='<img src="static/like.png"  width="50" height="50" onclick="likeVideo('+element.pk+')">'
        x+='<a id="like'+element.pk+'">'+element.fields.rating+'</a>'
        if (userStatus.status)
        {
            x+='<img src="static/images/remove.png"  width="50" height="50" onclick="removeVideo('+element.pk+')">'
        }
        x+='</div> '
    }
    document.getElementById("videos").innerHTML = x;
}

function updateLike()
{
    var request = new XMLHttpRequest();
    request.open("GET", "/api/getvidiospopular/", false);
    request.send();
    myObj = JSON.parse(request.responseText);
    var x='';
    for (element of myObj) {
        x='';
        x+='<a id="like'+element.pk+'">'+element.fields.rating+'</a>'
        var b='like'+element.pk+'';
        document.getElementById(b).innerHTML = element.fields.rating;
    }
}

function removeVideo(id)
{
    $.ajax({
        url: '/api/removevideoapi?id='+id,
        statusCode: {
            200: function (response) {
               document.getElementById('video'+id).remove();
            }
        },
    });
}