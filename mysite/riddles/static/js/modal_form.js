$(document).ready(function () {
    $('#buttonModel').click(function (event) {
        $('#overlay').fadeIn(400,
            function () {
                $('#modal_form')
                    .css('display', 'block')
                    .animate({ opacity: 1, top: '50%' }, 200);
            });
    });
    $('#modal_close, #overlay').click(function () {
        $('#modal_form')
            .animate({ opacity: 0, top: '45%' }, 200,
            function () {
                $(this).css('display', 'none');
                $('#overlay').fadeOut(400);
            }
            );
            document.getElementById('error').innerHTML='';
    });
    $('#sing_in').click(function () {
        function getCookie(name) {
            var cookieValue = null;
            if (document.cookie && document.cookie !== '') {
                var cookies = document.cookie.split(';');
                for (var i = 0; i < cookies.length; i++) {
                    var cookie = jQuery.trim(cookies[i]);
                    if (cookie.substring(0, name.length + 1) === (name + '=')) {
                        cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                        break;
                    }
                }
            }
            return cookieValue;
        }
        var csrftoken = getCookie('csrftoken');
        $.ajaxSetup({
            headers: { "X-CSRFToken": getCookie("csrftoken") }
        });
        $.ajax({
            type: "POST",
            dataType: 'json',
            contentType: 'application/json',
            url: "/api/login_in/",
            data: JSON.stringify({ 'username': document.getElementById("login").value, 'password1': document.getElementById("password1").value }),
            statusCode: {
                200: function (response) {
                    location.reload(true);
                },
                404:function (response) {
                    document.getElementById('error').innerHTML = 'Вы ввели неверный логин или пароль';
                }
            },
        });
    });
    $('#buttonLoginOut').click(function () {
        $.get('/api/logout', function (data) {
            location.reload(true);
        });
    });
});